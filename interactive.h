/* Oswiadczam, ze niniejsza praca stanowiaca podstawe
 * do uznania osiagniecia efektow uczenia sie
 * z przedmiotu SOP zostala wykonana przeze mnie samodzielnie.
 * Szymon Zygula 305931
 */

#ifndef INTERACTIVE_H
#define INTERACTIVE_H

#include "index.h"

void launch_interactive_console(indexing_data_t* data);
void print_command_prompt();

#endif
