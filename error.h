/* Oswiadczam, ze niniejsza praca stanowiaca podstawe
 * do uznania osiagniecia efektow uczenia sie
 * z przedmiotu SOP zostala wykonana przeze mnie samodzielnie.
 * Szymon Zygula 305931
 */

#ifndef ERROR_H
#define ERROR_H

#include <stdio.h>

#define ERR(source)\
    (perror(source),\
    fprintf(stderr, "%s:%d", __FILE__, __LINE__),\
    exit(EXIT_FAILURE))

#endif
